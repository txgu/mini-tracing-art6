# MiniTracing for ART

## Get Source

1. Make a directory and refer it as `$AOSP_HOME` for later usage.

        $ make AOSP_6.0.1_r77

2. Download AOSP, check out tag `android-6.0.1_r77`. You can follow instructions at <https://source.android.com/source/downloading>

        $ repo init -u https://android.googlesource.com/platform/manifest -b android-6.0.1_r77


3. Move `$AOSP_HOME/art` out of `$AOSP_HOME`.

        $ cd $AOSP_HOME && mv art ../art-origin

4. Download MiniTracing for ART. Note the default branch is `mini-tracing`.

        $ git clone https://bitbucket.org/txgu/mini-tracing-art6 $AOSP_HOME/art

## Build

You can follow instructions at <https://source.android.com/source/building>

1. Source build env

        $ source $AOSP_HOME/build/envsetup.sh

2. Launch configuration.

        $ launch aosp_arm-eng

3. Make

        $ make libart libart-compiler -j 8


Known Tool chain bug: <https://bbs.archlinux.org/viewtopic.php?id=209698>


## Install


1. Flash the factory image 6.0.1-M4B30Z to you Nexus 5. You can download the image at <https://developers.google.com/android/images#hammerhead>.

2. Connect your phone to your computer. Your phone must be rooted first.

3. You probably need to read `install_libart.sh` first.

        $ $AOSP_HOME/art/install_libart.sh


## Usage

Basically, you need first configure which app to be traced and than request the coverage harvest.
I have provided a command to enable and disable tracing, and harvest data.

Check <https://bitbucket.org/txgu/android-mt-cmd>.

## Parser

Check <https://bitbucket.org/txgu/android-mt-parser>
